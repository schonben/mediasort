<?php

class mediaSort {
	protected $file;
	protected $name;
	protected $year;
	protected $source;
	protected $destination;
	protected $target;


	public function __construct ($file,$source,$destination) {
		// Constructing the object does all the analysis, but doesn't perform any changes on disk.
		$this->file = $file;
		$this->source = $source.$file;
		$this->destination = $destination;
		if ($target = $this->isSeries($file)) {
			echo "Series: ".$target."\n";
		} else if ($target = $this->isMovie($file)) {
			echo "Movie: ".$target."\n";
		}
	}
	protected function isSeries ($file) {
		$path = $this->destination."Series/";
		if (preg_match("/(.+)(S[0-9]+E[0-9]+|[0-9]{1,2}(x|of)[0-9]{1,2}|20([0-9]{2}\.){3})/iu",$file,$m)) {
			$fullname = trim(str_replace("."," ",$m[1]));
			preg_match("/^([A-Z]{2,3}[0-9]? )?(.+)(20[0-9]{2}|[A-Z]{2})?$/",$fullname,$m);
			$this->name = trim($m[2]);
			
			$add="";
			if (isset($m[3]))
				$add = $m[3];
			
			if (!$this->getTarget($path,$this->name,$add)) {
				$this->target = $path.$this->name;
			}
			return $this->name;
		}
		return false;
	}
	protected function isMovie ($file) {
		$path = $this->destination."Movies/";
		if (preg_match("/(\[.+\])?(.+?)[({\[]?((19|20)[0-9]{2}|bdrip|dvdrip|brrip)[)}\]]?/i",$file,$m)) {
			$this->name = trim(str_replace("."," ",$m[2])," ([{-");
			if (is_numeric($m[3])) {
				$this->year = $m[3];
			}
			$fullname = "{$this->name} [{$m[3]}]";

			if ($this->getTarget($path,$this->name,$this->year)) {
				echo "Target Directory exists: {$this->target}\n";
				$this->target = "";
				return false;
			}
			$this->target = "{$path}{$this->name} [{$this->year}]";
			
			return $fullname;
		}
		return false;
	}
	protected function getTarget ($path,$search,$add) {
		foreach (array($path.$search,$path.$search." ".$add,$path.$search." [".$add."]") as $dir) {
			if (is_dir($dir)) {
				$this->target = $dir;
				return true;
			}
		}
        foreach (self::sdir($path) as $d) {
            if (preg_match("/^".$search." ?([0-9]{4}|[A-Z]{2})?$/i",$d,$m)) {
                if (is_dir("{$path}{$m[0]}")) {
                    $this->target = "{$path}{$m[0]}";
                    return true;
                }
            }
        }
        return false;
	}

	public function move() {
		// Perform the move.
		if (!empty($this->target) && !is_dir($this->target)) {
			echo "Creating directory: {$this->target}\n";
			mkdir($this->target);
		}
		if (is_dir($this->target)) {
			if (is_dir($this->source) && !empty($this->target)) {
				echo "Moving {$this->file} -> {$this->target}";
				self::moveRecursive($this->source,$this->target);
				echo "\n";
				return true;
			} else {
				echo "Moving {$this->file} -> {$this->target}";
				self::moveRecursive($this->source,self::pathConcat($this->target,$this->file));
				echo "\n";
				return true;
			}
		}
		return false;
	}
	public static function moveRecursive ($source,$path) {
		if (is_dir($source)) {
			// Item is a directory, create one at target and move contents with call to self.
			if (!is_dir($path))
				mkdir($path);
			foreach(self::sdir($source) as $record) {
				self::moveRecursive(self::pathConcat($source,$record),self::pathConcat($path,$record));
			}
			// Clean up source.
			rmdir($source);
		} else {
			// Item is a file, move it to new location.
			// I use the copy function to make it work better across disks.
			// TODO detect if on same disk and use move instead.
			if (copy($source,$path)) {
				// Only unlink if copy succeeded.
				unlink($source);
			}
		}
	}
	public static function sdir ($path) {
		// Return an array with all items in an directory. This is basically a filtered scandir call.
		$dir = array();
		foreach (scandir($path) as $record) {
			if ($record != "." && $record != "..")
				$dir[] = $record;
		}
		return $dir;
	}
	public static function pathConcat() {
		// Return a path concatenated from all arguments. Places one "/" character between parts.
		$path = "";
		foreach (func_get_args() as $arg) {
			$path = rtrim($path,"/ ")."/".ltrim($arg,"/ ");
		}
		return $path;
	}
}

?>
